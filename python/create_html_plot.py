"""New version 2019-09-06"""

import typing as tp

import numpy as np
import plotly
import plotly.io
import plotly.graph_objects as go
import PyTMM.refractiveIndex

import constants as const


COLOR_GAIN = "rgb(201, 24, 48)"
COLOR_BRF = "rgb(26, 38, 214)"
COLOR_ETALON = "rgb(111, 189, 34)"
COLOR_CAVITY = "rgb(240, 180, 53)"
COLOR_COMBINED = COLOR_ETALON
COLOR_OC = "rgb(0, 0, 0)"


def gain(target: float, width: float, width_level: float, gain_max: float):
    scale = np.arccos(width_level) * 2 / width

    def func(freq: tp.Union[float, np.ndarray]):
        return np.cos((freq - target) * scale) * gain_max

    return func


def brf_trans(fsr: float, target: float):
    """Approximate!"""

    def func(freq: tp.Union[int, np.ndarray]):
        return np.cos((freq - target) * 2 * np.pi / fsr) ** 2

    return func


def etalon_trans(refl1: float, refl2: float, l_eff: float, shift: float = 0):
    """Etalon transmission T

    https://en.wikipedia.org/wiki/Fabry%E2%80%93P%C3%A9rot_interferometer
    """
    const1 = (1 - refl1) * (1 - refl2)
    const2 = (1 - np.sqrt(refl1 * refl2)) ** 2
    const3 = 4 * np.sqrt(refl1 * refl2)

    def func(freq: tp.Union[float, np.ndarray]):
        freq2 = freq + shift
        wl = const.c / freq2
        phi = 2 * np.pi * l_eff / wl * 2
        return const1 / (const2 + const3 * np.sin(phi) ** 2)

    return func


def cavity_lines(fsr: float, wl_min: float, wl_max: float, x_scaler: float = 1, y_scaler: float = 1):
    center = (wl_max + wl_min) / 2
    num = np.floor((wl_max - wl_min) / fsr)
    wls = np.arange(start=center - num / 2 * fsr, stop=center + num / 2 * fsr, step=fsr) * x_scaler
    wls_nan = np.empty_like(wls)
    wls_nan[:] = np.nan
    wls_tripled = np.column_stack((wls, wls, wls_nan)).flatten()
    y = np.column_stack((np.zeros_like(wls), np.ones_like(wls) * y_scaler, wls_nan)).flatten()
    return go.Scattergl(
        x=wls_tripled,
        y=y,
        name="Cavity modes",
        mode="lines",
        visible="legendonly",
        line={
            "color": COLOR_CAVITY
        }
    )


# Misc

def freq2wl(freq):
    return const.c / freq


def wl2freq(wl):
    return const.c / wl


def main():
    # target = 269692.82e9
    target = 300e12
    target_wl = const.c / target
    print("Wavelength", target_wl)
    # Gain
    gain_max = 1.07
    gain_width = 10e12
    gain_width_level = 0.9
    # BRF
    # brf_fsr = 17e12   # Hz
    brf_fsr = 10e12
    # Etalon
    # etalon_fsr = 80e9
    etalon_thickness = 1.012e-3
    etalon_refl = 0.08
    # Cavity
    cavity_fsr = 1.2e9
    cavity_mode_max = 1.1
    # Output coupler
    oc_refl = 0.985

    plot_min = target - gain_width / 2
    plot_max = target + gain_width / 2
    plot_x = np.linspace(plot_min, plot_max, num=100000)
    plot_x_ghz = plot_x / 1e9

    # Gain
    gain_func = gain(target, width=gain_width, width_level=gain_width_level, gain_max=gain_max)
    gain_vec = gain_func(plot_x)
    gain_plot = go.Scattergl(
        x=plot_x_ghz,
        y=gain_vec * 100,
        name="Gain",
        mode="lines",
        line={
            "color": COLOR_GAIN
        }
    )
    gain_plot_dash = go.Scattergl(
        x=plot_x_ghz,
        y=gain_vec * 100,
        name="Gain",
        line={
            "dash": "dash",
            "color": COLOR_GAIN
        }
    )

    # BRF
    # https://www.filmetrics.com/refractive-index-database/Quartz
    # n_o = 1.54425
    # n_e = 1.55338
    brf_trans_func = brf_trans(fsr=brf_fsr, target=target)
    brf_trans_vec = brf_trans_func(plot_x)
    brf_plot = go.Scattergl(
        x=plot_x_ghz,
        y=brf_trans_vec * 100,
        name="BRF transmission",
        mode="lines",
        line={
            "color": COLOR_BRF
        }
    )
    brf_plot_dash = go.Scattergl(
        x=plot_x_ghz,
        y=brf_trans_vec * 100,
        name="BRF transmission",
        line={
            "dash": "dash",
            "color": COLOR_BRF
        }
    )

    # Etalon
    catalog = PyTMM.refractiveIndex.RefractiveIndex()
    yag = catalog.getMaterial("main", "Y3Al5O12", "Zelmon")
    n_yag = yag.getRefractiveIndex(target_wl * 1e9)
    print("n YAG", n_yag)
    etalon_trans_func = etalon_trans(etalon_refl, etalon_refl, l_eff=etalon_thickness * n_yag)
    etalon_trans_vec = etalon_trans_func(plot_x)
    etalon_plot = go.Scattergl(
        x=plot_x_ghz,
        y=etalon_trans_vec * 100,
        name="Etalon transmission",
        mode="lines",
        line={
            "color": COLOR_ETALON
        }
    )

    # Cavity
    cavity_plot = cavity_lines(fsr=cavity_fsr, wl_min=plot_min, wl_max=plot_max, x_scaler=1e-9, y_scaler=100*cavity_mode_max)

    combined_plot = go.Scattergl(
        x=plot_x_ghz,
        y=gain_vec * brf_trans_vec * etalon_trans_vec * oc_refl * 100,
        name="Gain * BRF * etalon * OC",
        mode="lines",
        line={
            "color": COLOR_COMBINED
        }
    )

    # Output coupler
    # oc_plot = go.Scattergl(
    #     x=[plot_min*1e-9, plot_max*1e-9],
    #     y=[oc_refl * 100]*2,
    #     mode="lines",
    #     line={
    #         "color": COLOR_OC
    #     },
    #     visible="legendonly",
    #     name="1.5 % output coupler"
    # )

    # 100 % horizontal
    plot_100 = go.Scattergl(
        x=[plot_min*1e-9, plot_max*1e-9],
        y=[100]*2,
        name="100 %",
        mode="lines",
        line={
            "color": COLOR_OC
        }
    )

    layout = go.Layout(
        title=go.layout.Title(
            text="VECSEL single-frequency operation and tuning"
        ),
        xaxis=go.layout.XAxis(
            title=go.layout.xaxis.Title(
                text="Frequency (GHz)"
            )
        ),
        yaxis=go.layout.YAxis(
            title=go.layout.yaxis.Title(
                text="%"
            )
        ),
    )

    fig = go.Figure(
        data=[gain_plot, brf_plot, etalon_plot, cavity_plot, plot_100],
        layout=layout
    )

    fig2 = go.Figure(
        data=[combined_plot, gain_plot_dash, brf_plot_dash, cavity_plot, plot_100],
        layout=layout
    )

    plotly.io.write_html(fig=fig, file="elements.html", include_plotlyjs=True)
    plotly.io.write_html(fig=fig2, file="combined.html", include_plotlyjs=True)


if __name__ == "__main__":
    main()
