import typing as tp

import matplotlib.pyplot as plt
import numpy as np
try:
    import PyTMM.refractiveIndex
except ImportError:
    PyTMM = None
import scipy.optimize

import constants as const
import plot_temp_dependence as plot_temp_dep


class Mirror:
    def __init__(self, name: str, refl: float, trans: float = None):
        self.name = name
        self.refl = refl
        self.trans = 1 - refl if trans is None else trans


class Etalon:
    def __init__(
            self, wl: float, thickness: float,
            r1: float, r2: float = None,
            material: str = None, refr_ind_source: str = None, refr_ind: float = None,
            refr_ind_surrounding: float = 1,
            temp_shift: float = 0):
        if temp_shift > 0:
            raise ValueError("Temperature shift coefficient should be negative!")

        if refr_ind is None:
            if PyTMM is None:
                raise RuntimeError("PyTMM is required for automatic setup of the refractive index")
            catalog = PyTMM.refractiveIndex.RefractiveIndex()
            material = catalog.getMaterial("main", material, refr_ind_source)
            self.refr_ind = material.getRefractiveIndex(wl * 1e9)
        else:
            self.refr_ind = refr_ind

        self.wl = wl
        self.thickness = thickness
        self.temp_shift = temp_shift
        self.refr_ind_rel = self.refr_ind / refr_ind_surrounding
        self.r1 = r1
        self.r2 = r1 if r2 is None else r2

    def trans(self, freq: tp.Union[float, np.ndarray], theta: float = 0, temp: float = 0):
        freq_shifted = freq + temp*self.temp_shift
        phi = 2*np.pi*self.refr_ind_rel*freq_shifted / const.c * self.thickness * np.cos(theta)
        return (1-self.r1)*(1-self.r2) / ((1-np.sqrt(self.r1*self.r2))**2 + 4*np.sqrt(self.r1*self.r2)*np.sin(phi)**2)


class BRF:
    def __init__(self, wl: float, fsr_f: float, temp_shift: float = 0):
        """
        :param fsr_f: free spectral range (Hz)
        :param temp_shift: temperature shift (Hz/K)
        """
        self.wl = wl
        self.fsr_f = fsr_f
        if temp_shift < 0:
            raise ValueError("Temperature shift coefficient should be positive!")
        self.temp_shift = temp_shift

    def trans(self, freq: tp.Union[float, np.ndarray], temp: float = 0):
        return np.cos((freq + temp*self.temp_shift) * 2*np.pi / self.fsr_f)**2


class Chip:
    def __init__(self, wl: float, gain_width_wl: float, temp_shift_wl: float = 0, temp0: float = 273):
        self.wl = wl
        self.freq = const.c / wl
        self.gain_width_wl = gain_width_wl
        self.gain_width_f = const.c / (self.wl - gain_width_wl/2) - const.c / (self.wl + gain_width_wl/2)
        self.temp_shift = wl**2 * const.c / temp_shift_wl
        self.temp0 = temp0

    def gain(self, freq: tp.Union[float, np.ndarray], temp: float = 0):
        freq = np.atleast_1d(freq)
        ret = 1 - ((freq - self.freq + (temp-self.temp0)*self.temp_shift)/(2*self.gain_width_f))**2
        ret[ret < 0] = 0
        return ret


class Laser:
    def __init__(
            self,
            chip: Chip, etalon: Etalon, brf: BRF, oc: Mirror, l1: float,
            folding: Mirror = None, l2: float = 0, piezo_coeff: float = 0):
        self.chip = chip
        self.etalon = etalon
        self.brf = brf
        self.oc = oc
        self.folding = folding
        self.l1 = l1
        self.l2 = l2
        self.fsr = 0.5 * const.c / (self.l1 + self.l2)
        self.piezo_coeff = piezo_coeff

    def cavity_trans(self, freq: tp.Union[float, np.ndarray], v_piezo: float):
        n = 5
        fsr = 0.5 * const.c / (self.l1 + self.l2 + v_piezo*self.piezo_coeff)
        return np.cos(np.pi * freq / fsr)**(2*n)

    def operation(self):
        pass

    def gen_freq_and_gain(self, wl_range):
        freq_vec = np.linspace(const.c / wl_range[0], const.c / wl_range[1], 100000)
        print("Freq vec size:", freq_vec.size)

        def freq_and_gain(vec: np.ndarray):
            """Returns f, gain"""
            chip = self.chip.gain(freq_vec, vec[0])
            brf = self.brf.trans(freq_vec, vec[1])
            etalon = self.etalon.trans(freq_vec, 0, vec[2])
            cavity = self.cavity_trans(freq_vec, vec[3])
            ret = chip * brf * etalon * cavity
            max_ind = np.argmax(ret)
            return freq_vec[max_ind], ret[max_ind]
        return freq_and_gain


def main():
    wl = 1178e-9
    chip = Chip(wl=wl, gain_width_wl=20e-9, temp_shift_wl=1e-9)
    brf = BRF(wl=wl, fsr_f=10e12, temp_shift=37e9)
    etalon = Etalon(wl=wl, thickness=1.012e-3, r1=0.08, material="Y3Al5O12", refr_ind_source="Zelmon", temp_shift=-3.4e9)

    oc = Mirror("O116L005", 0.985)
    # folding = Mirror("R0607075", 0.998)

    laser = Laser(chip=chip, etalon=etalon, brf=brf, oc=oc, l1=0.1, piezo_coeff=3.3e-6/200)
    plot_temp_dep.plot_all(laser)

    freq_and_gain = laser.gen_freq_and_gain((wl-10e-9, wl+10e-9))
    freq = const.c / wl

    def penalty(vec):
        f, g = freq_and_gain(vec)
        return np.abs(f - freq) - g*1e9

    xks = []
    states = []

    def callback(xk):
        xks.append(xk)
        # states.append(state)
        return False

    # Todo: this does not work yet
    sol = scipy.optimize.minimize(penalty, x0=np.array([0, 0, 0, 0]), callback=callback)
    print(sol)
    print("xks")
    for xk in xks:
        print("-", xk)

    plt.show()


if __name__ == "__main__":
    main()
