import matplotlib.pyplot as plt
import numpy as np

import constants as const
from simulation import BRF, Chip, Etalon, Laser


def plot_chip(chip: Chip, ax: plt.Axes):
    wl_vec = np.linspace(chip.wl-10e-9, chip.wl+10e-9, 1000)
    wl_vec_nm = wl_vec*1e9
    freq_vec = const.c / wl_vec
    t0 = 20 + 273
    t1 = 21 + 273
    t2 = 22 + 273
    ax.plot(wl_vec_nm, chip.gain(freq_vec, t0), label=f"{t0} K")
    ax.plot(wl_vec_nm, chip.gain(freq_vec, t1), label=f"{t1} K")
    ax.plot(wl_vec_nm, chip.gain(freq_vec, t2), label=f"{t2} K")
    ax.legend()
    ax.set_xlabel("λ (nm)")
    ax.set_ylabel("Gain")
    ax.set_title("Chip")


def plot_brf(brf: BRF, ax: plt.Axes):
    t0 = 20 + 273
    t1 = 30 + 273
    t2 = 40 + 273
    wl_vec = np.linspace(brf.wl-10e-9, brf.wl+10e-9, 1000)
    wl_vec_nm = wl_vec*1e9
    freq_vec = const.c / wl_vec
    ax.plot(wl_vec_nm, brf.trans(freq_vec, t0), label=f"{t0} K")
    ax.plot(wl_vec_nm, brf.trans(freq_vec, t1), label=f"{t1} K")
    ax.plot(wl_vec_nm, brf.trans(freq_vec, t2), label=f"{t2} K")
    ax.legend()
    ax.set_xlabel("λ (nm)")
    ax.set_ylabel("T")
    ax.set_title("BRF")


def plot_etalon(etalon: Etalon, ax: plt.Axes):
    t0 = 20 + 273
    t1 = 25 + 273
    t2 = 30 + 273
    wl_vec = np.linspace(etalon.wl-1e-9, etalon.wl+1e-9, 1000)
    wl_vec_nm = wl_vec*1e9
    freq_vec = const.c / wl_vec
    ax.plot(wl_vec_nm, etalon.trans(freq_vec, 0, t0), label=f"{t0} K")
    ax.plot(wl_vec_nm, etalon.trans(freq_vec, 0, t1), label=f"{t1} K")
    ax.plot(wl_vec_nm, etalon.trans(freq_vec, 0, t2), label=f"{t2} K")
    ax.legend()
    ax.set_xlabel("λ (nm)")
    ax.set_ylabel("T")
    ax.set_title("Etalon")


def plot_cavity(laser: Laser, ax: plt.Axes):
    v0 = 0
    v1 = 5
    v2 = 10
    wl_vec = np.linspace(laser.chip.wl - 0.01e-9, laser.chip.wl + 0.01e-9, 1000)
    wl_vec_nm = wl_vec * 1e9
    freq_vec = const.c / wl_vec
    print("Frequency step for plotting cavity:", freq_vec[1] - freq_vec[0])
    ax.plot(wl_vec_nm, laser.cavity_trans(freq_vec, v0), label=f"{v0} V")
    ax.plot(wl_vec_nm, laser.cavity_trans(freq_vec, v1), label=f"{v1} V")
    ax.plot(wl_vec_nm, laser.cavity_trans(freq_vec, v2), label=f"{v2} V")
    ax.legend()
    ax.set_xlabel("λ (nm)")
    ax.set_ylabel("T")
    ax.set_title("Cavity")


def plot_all(laser: Laser):
    fig = plt.figure()
    plot_chip(laser.chip, fig.add_subplot(221))
    plot_brf(laser.brf, fig.add_subplot(222))
    plot_etalon(laser.etalon, fig.add_subplot(223))
    plot_cavity(laser, fig.add_subplot(224))
