# MAT-60456 Optimization Methods modeling exercise: wavelength tuning of a single-frequency VECSEL

Mika Mäki, 2019

### Libraries

- [PyTMM](https://github.com/kitchenknif/PyTMM)
- [refractiveindex.info database](https://refractiveindex.info/download.php)
