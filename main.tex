\documentclass[a4paper, english, 12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage[
	backend=biber,
	sorting=none,
	sortcites,
]{biblatex}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{placeins}
\usepackage{subcaption}
\usepackage{svg}

\title{MAT-60456 modeling exercise: wavelength tuning of a single-frequency VECSEL}
\author{Mika Mäki}

\addbibresource{references.bib}

\begin{document}

\maketitle

\subsection*{Notice}
This document and the related code are available at \newline
\href{https://gitlab.com/AgenttiX/mat-60456-project}{https://gitlab.com/AgenttiX/mat-60456-project}. \newline
There are also interactive figures in the folder \textit{fig\_interactive}, and they can help significantly in understanding the problem. (However, please note that the figures have so many data points that they may not be visible with all browsers.)
The scripts require the \href{https://github.com/kitchenknif/PyTMM}{PyTMM} library and \href{7https://refractiveindex.info/about}{RefractiveIndex.info} database to work, and you can download these by clicking the hyperlinks in this sentence.

\section{Introduction}

Lasers are among the most important tools for the control of atomic transitions.
However, not just any laser will do, but there are several requirements on the properties of the laser light.
First of all the laser has to emit light exactly at the transition wavelength, and the power of the laser beam has to be sufficient.
These conditions already filter out several laser types, such as gas lasers, since they are based on atomic transitions and therefore can only emit on very specific wavelengths.

The linewidth, also known as the frequency spectrum, has to be narrow enough, that other states with similar energies are not affected.
Since frequency noise is connected to amplitude noise by the Fourier transformation, this also requires that the amplitude of the electric field of the laser beam, and therefore the power, should be very stable.
The divergence of the beam should also be as small as possible, preferably diffraction-limited.

Among the few laser laser types that can fulfill these requirements are the vertical-external-cavity surface-emitting lasers, also known as VECSELs or semiconductor disk lasers.
However, tuning a VECSEL to the target wavelength and simultaneously achieving all the other restrictions is far from trivial, although simpler than with many other laser types, and requires careful optimization to maintain all the necessary constraints.

This topic was chosen, since the author works at the start-up company \href{https://vexlum.com/}{Vexlum Ltd} that manufactures single-frequency VECSELs, and since there is high interest in automating the tuning of the VECSEL to the target wavelength.


\section{Principles of single-frequency VECSELs}

A VECSEL consists of a gain mirror and one or more external mirrors, which are aligned to form a standing wave resonator known as laser cavity.
The energy is provided by another laser called the pump laser, and its light is focused to a spot on the surface of the gain mirror.
This causes the gain mirror to emit light spontaneously, and thanks to the resonator some of the light returns to the same spot where it was emitted, and this gives rise to stimulated emission.
\iffalse
In stimulated emission a photon interacts with an electron that is excited to a state with the same energy as the photon.
This causes the electron to drop to a lower energy state, and its energy is emitted in the form of another photon, which has exactly the same properties as the first photon.
This continues exponentially until the populations of the electrons on the ground state and the excited state 
\fi
The simplest VECSELs have so-called I-cavities, which have only one external mirror.
An example of such I-cavity is provided in figure \ref{fig:I-cavity}.
However, for single-frequency operation the wavelength has to be filtered by several optically active elements in the cavity, and to fit these elements in the cavity an additional mirror is needed.
This is called a V-cavity, and an example is provided in figure \ref{fig:V-cavity}.
In addition to the wavelength-selective elements the cavity in figure \ref{fig:V-cavity} has a nonlinear crystal for frequency-doubling of the single-frequency light.
However, this frequency doubling is not covered in this modeling exercise.

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{0.38\textwidth}
	\fontsize{9}{10}\selectfont
	\includesvg[width=\textwidth]{fig/I-cavity.svg}
	\caption{I-cavity}
	\label{fig:I-cavity}
\end{subfigure}
~
\begin{subfigure}[b]{0.58\textwidth}
	\fontsize{9}{10}\selectfont
	\includesvg[width=\textwidth]{fig/V-cavity.svg}
	\caption{V-cavity}
	\label{fig:V-cavity}
\end{subfigure}
\caption{Common VECSEL cavity types \cites{bsc}{guina_optically_2017}}
\end{figure}

The wavelength-selective elements include a \href{https://en.wikipedia.org/wiki/Fabry\%E2\%80\%93P\%C3\%A9rot_interferometer}{Fabry-Perot resonator}, also known as an \href{https://www.rp-photonics.com/etalons.html}{etalon}, and a \href{https://www.rp-photonics.com/birefringent_tuners.html}{birefringent filter}, abbreviated as BRF.
The transmission of the Fabry-Perot resonator is a periodic function, and the transmittivity is at its highest when all the reflections inside the resonator are in phase so that they interfere constructively.

The birefringent filter is an element for which the refractive depends on the polarization of the light.
It is placed in the cavity at the \href{https://en.wikipedia.org/wiki/Brewster\%27s_angle}{Brewster's angle}, so there is no reflection at its surfaces.
This results in a highly complex and polarization-dependent transmission.
However, the transmission is approximately sinusoidal for the optimal polarization angle.

As with any standing-wave resonator, a laser cavity can only accommodate laser modes with certain discrete wavelengths.
This is due to the requirement that there have to be nodes at both ends of the cavity.
However, the length of the cavity can be adjusted by mounting one of the mirrors to a \href{https://en.wikipedia.org/wiki/Piezoelectricity}{piezoelectric} actuator, the length of which can be adjusted by a simple change in voltage.

\FloatBarrier
As an additional note for an interested reader, a schematic of the layers and the band structure of the gain mirror is given in figure \ref{fig:structure}.
The electric field of the laser light is marked as red.

\begin{figure}[ht!]
\centering
% Font size bypass for the SVG import
\fontsize{9}{10}\selectfont
\includesvg[width=\textwidth]{fig/structure.svg}
\caption{Structure of a VECSEL gain mirror \cites{bsc}{guina_optically_2017}}
\label{fig:structure}
\end{figure}



\FloatBarrier
\section{Modeling the problem}

The primary target is to maximise the output power of the laser at the target wavelength by changing the available control parameters. Mathematically this can be expressed as
\begin{equation}
\max_{f = f_0} \ P (P_\text{pump}, t_\text{chip}, t_\text{BRF}, t_\text{etalon}, V_\text{piezo}, \theta_{BRF}).
\end{equation}

The symbol $t$ is used for temperature, since the symbol $T$ will be reserved for transmittance.
$f_0$ denotes the target frequency (corresponding to the target wavelength), and $V_\text{piezo}$ is the voltage of the piezoelectric element.
Each of these parameters has a fixed range determined by the physical limitations of the device.
In addition, it would be preferable to operate the laser at reasonable temperatures and voltages, so the deviation of the parameters from their original setpoints could be included in the cost function.

As a more physically accurate alternative to the power we can use the single-pass gain of the cavity, which is the ''multiplication factor'' of the photon count over a single round-trip in the cavity.
Maximising gain is pretty much equivalent to maximising the output power, so we can write similarily
\begin{equation}
\max_{f = f_0} \ G_\text{total} (P_\text{pump}, t_\text{gain}, t_\text{BRF}, t_\text{etalon}, V_\text{piezo}, \theta_{BRF}).
\end{equation}
The equivalent minimization problem is
\begin{equation}
\min \ -G_\text{total} (P_\text{pump}, t_\text{gain}, t_\text{BRF}, t_\text{etalon}, V_\text{piezo}, \theta_\text{BRF}, f_0).
\end{equation}
Since the ranges for the control parameters are finite, these problems are subject to the constraints
\begin{align}
P_\text{pump} - P_\text{pump, max} \leq 0 \\
t_\text{gain} - t_\text{gain, max} \leq 0 \\
t_\text{BRF} - t_\text{BRF, max} \leq 0 \\
t_\text{BRF, min} - t_\text{BRF} \leq 0 \\
t_\text{etalon} - t_\text{etalon, max} \leq 0 \\
t_\text{etalon, min}  - t_\text{etalon} \leq 0 \\
V_\text{piezo} - V_\text{piezo, max} \leq 0 \\
\theta_\text{BRF} - \theta_\text{BRF, max} \leq 0 \\
\theta_\text{BRF, min} - \theta_\text{BRF} \leq 0
\end{align}
Additionally it can be noted that
\begin{equation}
P_\text{pump}, t_\text{gain}, t_\text{BRF}, t_\text{etalon}, V_\text{piezo} \geq 0
\label{eq_greater}
\end{equation}
It should be noted that the BRF angle $\theta_\text{BRF}$ can be negative depending on the choice of reference, but for it to satisfy equation \ref{eq_greater} as well it can either be described by a subtraction of two variables, or a suitable offset can be applied to the reference.

The gain can be expressed as a product of the gain of the gain chip and the transmissions of the cavity elements so that
\begin{equation}
G_\text{total} = 
G_\text{chip}(f, P_\text{pump}, t_\text{chip})
\cdot T_\text{BRF}(f, t_\text{brf}, \theta_{BRF})
\cdot T_\text{etalon}(f, t_\text{etalon})
\cdot T_\text{cavity}(f)
\cdot R_\text{OC},
\end{equation}
where $R_\text{OC}$ is the reflectivity of the output coupler.
Output coupler is the mirror through which the laser output is extracted, so that's why it's only partially reflective.
In other words, the gain chip adds photons to the cavity, and the other elements remove some of the photons.

\iffalse
The cavity transmission $T_\text{cavity}$ differs from unity due to the imperfect reflectivity of other other cavity mirrors such as the folding mirror and the absorption of air inside the cavity.
The absorption occurring inside the chip can be either included in the gain of the chip or in the cavity transmission.
\fi

For the laser to operate, the total gain has to overcome the losses so that
\begin{equation}
G_\text{total} \geq 1.
\end{equation}
If some of the wavelength-selective elements would not be in place in the cavity, this condition would be true for several modes allowed by the cavity length, and we would have multi-mode operation.
It should also be noted that in steady-state operation the gain is exactly equal to the losses, since the gain of the chip decreases to prevent the photon count and therefore power from diverging towards infinity.
However, when modeling the problem we would like to operate the gain chip so that we could have as much gain as possible, if necessary, so this restriction is left out from the model.


\subsection{Tuning process}

The control parameters have various effects to the laser behaviour with different magnitudes.
Usually starting the laser for the first time consists of the following steps.
\begin{itemize}
\item Set BRF and etalon temperatures above room temperature and the piezo to a reasonable voltage so that they can be adjusted in both directions.
\item Turn on the laser and find reasonable $P_\text{pump}$, $t_\text{gain}$ and $\theta_\text{BRF}$ so that the laser operates
\item Set the laser to an approximately correct wavelength range by changing $\theta_\text{BRF}$.
\item Optionally adjust the etalon angle to optimize the output power and wavelength
\end{itemize}

Correspondingly fine-tuning the laser wavelength usually consists of the following steps. \cite{valo_sf_manual}
\begin{itemize}
\item Further adjust the coarse wavelength range by changing $t_\text{BRF}$.
\item Fine-tune the wavelength range by changing $t_\text{etalon}$.
\item Select the individual cavity mode by adjusting $V_\text{piezo}$.
\end{itemize}

Ideally the automatic tuning system could do both fine and coarse tuning, but even a system capable of only fine tuning would be very useful.


\subsection{Gain chip}

A simple model for the gain of the gain chip would be a downwards-opening parabola.
Using this model,
\begin{equation}
\begin{aligned}
& G_\text{chip}(f, P_\text{pump}, t_\text{chip}) \\
& \approx a \cdot P_\text{pump} - b \cdot t_\text{chip} - (f - f_\text{chip} + c \cdot P_\text{pump} + d \cdot (t_\text{chip} - t_{\text{chip}, 0}))^2,
\end{aligned}
\end{equation}
where $a, b, c$ and $d$ are constants determined by the material properties of the chip, $f_\text{chip}$ is the frequency of the gain maximum at small pumping and $t_{\text{chip}, 0}$ is an ideal operating temperature of the gain chip.
The pumping increases the gain quite linearly, but it also shifts the gain to longer wavelengths.
Increasing the temperature also causes a shift to longer wavelengths.

In a more realistic case the actual temperature of the chip depends on the pump power as well, since there is some thermal resistance between the chip and the temperature sensor, and the temperature of the sensor is kept constant by a Peltier element (TEC).


\subsection{Birefringent filter}

The behaviour of the birefringent filter is complex, but we could approximate it as a modified cosine.
A more detailed but qualitative analysis can be found at \href{https://www.rp-photonics.com/birefringent_tuners.html}{RP photonics}.
An approximative function would be of the form
\begin{equation}
T_\text{BRF} \approx a + b \cos(f + c(\theta - \theta_0) + d(t - t_0)) + e - gf^2 - h(\theta - \theta_0)^2,
\end{equation}
where $a, b, c, d, e, g$ and $h$ are constants determined by the properties of the BRF.
They should be set so that the maximum of this function is 1 when $\theta = \theta_0$.
This function is a cosine that has been shifted upwards by the constant factor $b$, and penalised by a quadratic function.
The penalty arises from the fact that the further away the BRF is from the ideal angle, the poorer is its refractive index contrast.
The frequency dependence of the transmission can be shifted by a change in the temperature.


\subsection{Etalon}

The transmission of an etalon can be found from several textbooks to be
\begin{equation}
T_\text{etalon} = \frac{(1-R_1)(1-R_2)}{(1-\sqrt{R_1 R_2})^2 + 4\sqrt{R_1 R_2} \sin^2 (\phi)}.
\end{equation}

The symbols $R_1$ and $R_2$ are the reflectivities of the etalon surfaces, and $2\phi$ is the phase difference between successive multiple reflections given by the equation

\begin{equation}
\phi = kL \cos \theta = \frac{2\pi n_r f}{c} L \cos \theta,
\end{equation}

where $L$ is the thickness of the etalon, $\theta$ is the propagation angle of light inside the etalon, and $n_r$ is the refractive index of the etalon relative to the refractive index of the surrounding medium. \cite[s.143--144]{svelto_principles_2010}
In the simulations we can assume that $\theta = 0$.
The way we can control the overall transmission is by changing $L$ by modifying the temperature of the etalon.
The thermal expansion of the etalon can be considered as linear so that $L = (1 + \alpha_L(t - t_0))L_0$, where $\alpha_L$ is the thermal expansion coefficient of the etalon.


\subsection{Cavity}

The transmission of the cavity is a frequency comb, which is unity at the wavelengths allowed by the resonator length $d$ and zero otherwise.
The term frequency comb refers to the plot of the function, which is roughly a set of vertical lines connected by a horizontal line.
The free spectral range of the cavity is
\begin{equation}
\Delta f_{FSR} = \frac{c}{2(L_1 + L_2 + a \cdot V_\text{piezo})},
\end{equation}
where $c$ is the speed of light, $d$ is the cavity length, and $L_1$ and $L_2$ are the lengths of the cavity arms and a is a coefficient for the expansion of the piezoelectric element.
It should be noted that the sign of $a$ depends on whether the cavity length increases or decreases when the piezo voltage is increased.
Using these values the transmission can be expressed as
\begin{align}
T_\text{cavity} &=
\begin{cases}
1 \qquad \text{when} \ \lambda = \frac{2d}{k}, k \in \mathbb{Z}_+) \\
0 \qquad \text{otherwise}
\end{cases} \\
&= \begin{cases}
1 \qquad \text{when} \ f = \frac{kc}{2d} \\
0 \qquad \text{otherwise}
\end{cases} \\
&\approx \cos(\frac{\pi f}{\Delta f_{FSR}})^{2n},
\end{align}
where $n$ is a positive integer that is numerically suitable for the optimization algorithm.

The imperfect reflectivity of the in-cavity mirrors such as the folding mirror and the absorption of air could be accounted for by multiplying the cavity transmission by a wavelength-dependent factor. Please also note that in the interactive figures the cavity modes are so tightly spaced that extensive zooming is required to distinguish them from each other.
These interactive figures can be found in the Git repository.


\subsection{Additional restrictions}

Additional restrictions arise if the tuning of the laser from one wavelength to another is required to be continuous except at predetermined steps, when the laser is allowed to switch to another etalon mode.
In other words, during the optimization the maximum of $G$ should move continuously to prevent the laser from jumping to another mode except when specifically allowed to do so.
The jump to another etalon mode is required, since continuous scanning requires continuous adjustment of the piezo voltage, which has its limits.
This kind of operation is required for sweeping over a range of frequencies, and it's highly useful for determining the frequency response of a material very accurately.
For the scanning to continue the laser would have to be restored after the jump to the previous wavelength with the same output power, and this is very difficult to do manually.
Therefore an automated solution is needed.

The gain should also always overcome the losses ($G_\text{total} \geq 1$) so that the laser would not stop lasing during the optimization.
If the laser would stop lasing, it would have a significant effect on the temperature of the chip, since the power that was previously emitted as light would be mostly absorbed.
In the worst case this could permanently damage the chip, and in most cases a temporary decrease in the pump power is necessary to restart the lasing.


\section{Optimizing the model}

Optimizing the model has a few challenges.
First of all the transmissions are far from convex due to their trigonometric behaviour, and there are numerous local minima.
Therefore merely solving for the zeroes of the derivative or using one of the classical numerical algorithms such as Newton's method are not feasible options, although they could be used as a parts of a more complex algorithm.

Second, the cavity modes are so close to each other, that if using the sinusoidal approximation described above, distinguishing them would require very high numerical precision and small optimization step sizes.
To give a scale of this, the width of the gain spectrum of the chip is in the THz range, the spacing between each cavity mode is in the GHz range, and the piezo voltage can be used to adjust the cavity modes in the MHz range.
Therefore a brute-force approach for finding the operating wavelength and corresponding total gain would require evaluating the gain at millions of wavelength points with each combination of the control parameters.

Third, the status of the wavelength-selective elements is not known exactly, since only the effect of adjusting the control parameters can be evaluated.
Therefore the values for the angles and element thicknesses are approximate, and the coefficients of temperature and voltage dependences have to be determined empirically.
As a result, any simulation can only give hints on the correct direction for chancing the parameters, not exact values to which to set them.

A naive Python-based simulation and figure-generating script are available in the Git repository in the file \textit{simulation.py}.
By adjusting the input parameters it can be seen that the limitations of numerical precision result in errors with the SciPy-based optimization.

To overcome the aforementioned issues the following might be a feasible starting point for simulating the problem:
\begin{itemize}
\item Find the optimum of $G_\text{chip} \cdot T_\text{BRF}$ and restrict the search close to this value for performance.
\item Evaluate $G_\text{total}$ at each cavity mode in the aforementioned range, and choose the highest. The emission wavelength is the wavelength of this mode.
\item Use an appropriate penalty function to evaluate the laser performance. The function $a \cdot (f-f_\text{target})^2 - b \cdot G_\text{total}$, where $a$ and $b$ are constants, might be suitable.
\item Give this penalty as a feedback to the optimization algorithm
\item Use the optimization algorithm to decide new values for the control parameters.
\end{itemize}

With this kind of a simulation the feedback ($f$, $G_\text{total}$) would be in the same format as from a real laser, and therefore the simulation might provide a suitable testing ground for different optimization algorithms.
The supervisors of the author have been enthusiastic about using machine learning for this optimization, and it has been suggested that the author could possibly do a PhD thesis on the subject.
However, the author is a bit sceptical regarding the applicability of such black-box methods, since they would have to be able to cope with possible drifts and otherwise changing responsens of the laser, whereas in more continuous models finding the new optimum after such changes would hopefully result in only small changes to the control parameters during the optimizateion.
Therefore splitting the optimization to several steps and carefully selecting the region of interest as described above might be the best way to tackle the problem, but it is likely that the best optimization algorithms can only be found by practical testing of several variations.
In any case the author would be very thankful for any comments and recommendations on how to approach this optimization problem.

\pagebreak
\printbibliography

\end{document}
